import {MigrationInterface, QueryRunner} from "typeorm";

export class AddUserAge1578665404539 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE User ADD COLUMN age INTEGER`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE User DROP COLUMN age`);
    }

}
