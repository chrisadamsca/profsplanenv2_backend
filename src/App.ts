import * as express from 'express';
import {createConnection} from "typeorm";
import { ApiProjects } from './api/projects';
import { ApiUsers } from './api/users';
import { ApiContactpersons } from './api/contactpersons';
import Auth from './api/auth';
import * as cookieParser from "cookie-parser";
import * as bodyParser from "body-parser";
import * as cors from 'cors';

class App {
  public express;
  public connection;

  constructor () {
    this.express = express();
    this.mountRoutes();
    this.connection = createConnection();
  }

  private mountRoutes (): void {
    this.express.use(bodyParser.json());
    this.express.use(cookieParser());
    this.express.use(cors({
      origin: 'http://localhost:4200',
      credentials: true
    }));

    const router = express.Router();

    //---Projects API---
    //------------------

    // SELECT all Projects
    router.get('/projects', Auth.authenticate(), (req, res) => {
      ApiProjects.getAll().then(projects => {
        res.json(projects);
      });
    });

    // SELECT all published Projects (status: true)
    router.get('/projects/published', (req, res) => {
      ApiProjects.getAllPublished().then(projects => {
        res.json(projects);
      });
    });

    // SELECT Project by id
    router.get('/projects/:id', Auth.authenticate(), (req, res) => {
      ApiProjects.getOne(req.params.id).then(projects => {
        res.json(projects);
      });
    });

    // INSERT Project
    router.post('/projects', Auth.authenticate(), (req, res) => {
      ApiProjects.add(req.body).then(answer => {
        res.send(answer);
      });
    });

    // DELETE Project by id
    router.delete('/projects/:id', Auth.authenticate(), (req, res) => {
      ApiProjects.delete(req.params.id).then(answer => {
        res.send(answer);
      });
    });

    // UPDATE Project by id
    router.put('/projects', Auth.authenticate(), (req, res) => {
      ApiProjects.update(req.body).then(answer => {
        res.send(answer);
      });
    });

    //---ContactPerson API---
    //-----------------------

    // SELECT all Contactpersons
    router.get('/contactpersons', Auth.authenticate(), (req, res) => {
      ApiContactpersons.getAll().then(contactpersons => {
        res.json(contactpersons);
      });
    });

    // SELECT Contactperson by id
    router.get('/contactpersons/:id', Auth.authenticate(), (req, res) => {
      ApiContactpersons.getOne(req.params.id).then(contactperson => {
        res.json(contactperson);
      });
    });

    //---User API---
    //--------------

    // INSERT User
    router.post('/users/', (req, res) => {
      ApiUsers.add(req.body).then(answer => {
        res.send(answer);
      });
    });

    // SELECT User by id
    router.get('/users/:id', Auth.authenticate(), (req, res) => {
      ApiUsers.getOne(req.params.id).then(users => {
        res.json(users);
      });
    });

    //---Authentication API---
    //------------------------

    // Authenticate User
    router.post('/login', (req, res) => {
      Auth.login(req.body).then(token => {
        if(!token) {
          res.sendStatus(401);
        } else {          
          res.cookie("SESSIONID", token, {
              httpOnly: false, 
              secure: false
            }).end();
        }
      });
    });

    // check if User is logged in
    router.get('/login', Auth.authenticate(), (req, res) => {
      res.send(true);
    });

    // logout user
    router.get('/logout', Auth.authenticate(), (req, res) => {
      Auth.logout().then(token => {       
      res.cookie("SESSIONID", token, {
          httpOnly: false, 
          secure: false
        }).end();
      });

    });

    this.express.use('/api', router);
  }
}

export default new App().express;