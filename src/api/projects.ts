import { getConnection } from "typeorm";
import { Project } from "../entity/Project";
import { Contactperson } from "../entity/Contactperson";
import { ProjectBody } from "./models";

export class ApiProjects {

    private constructor() {};

    // GET all Projects
    public static async getAll(): Promise<Project[]> {
        const projects = await getConnection().getRepository(Project).find({relations: ["contactperson"]});
        return projects;
    }

    // GET Project by id
    public static async getOne(id: number): Promise<Project> {
        const project = await getConnection().getRepository(Project).findOne({relations: ["contactperson"], where: { id: id }});
        return project; 
    }

    // GET all published Projects
    public static async getAllPublished(): Promise<Project[]> {
        const projects = await getConnection().getRepository(Project).find({status: true});
        return projects; 
    }

    // INSERT given Project
    public static async add(newProject: ProjectBody): Promise<boolean> {
        let contactpersonRepository = await getConnection().getRepository(Contactperson);
        let projectRepository = await getConnection().getRepository(Project);
        const fetchedProject = await projectRepository.findOne({title: newProject.title});
        
        if(fetchedProject){
            return false;

        } else {
            if(newProject.contactperson) {
                const newContactperson = newProject.contactperson;
                const contactperson = new Contactperson(newContactperson.firstname, newContactperson.lastname, newContactperson.email, newContactperson.date, newContactperson.comment);
                await contactpersonRepository.save(contactperson);

                const project = new Project(newProject.title, newProject.lecturer, newProject.professor, newProject.description, newProject.status, contactperson);
                await projectRepository.save(project);

            } else {
                const project = new Project(newProject.title, newProject.lecturer, newProject.professor, newProject.description, newProject.status);
                await projectRepository.save(project);
            }
              
            return true;
        } 
    }

    // DELETE given Project
    public static async delete(id: number): Promise<boolean> {
        let projectRepository = await getConnection().getRepository(Project);
        let contactpersonRepository = await getConnection().getRepository(Contactperson);
        let fetchedProject = await projectRepository.findOne({relations: ["contactperson"], where: { id: id }});
        if(fetchedProject) {
            projectRepository.delete({id: fetchedProject.id});
            contactpersonRepository.delete({id: fetchedProject.contactperson.id});
        }
        
        return true;
    }

    // Update Project
    public static async update(updateProject: ProjectBody): Promise<boolean> {        
        let projectRepository = await getConnection().getRepository(Project);
        let contactpersonRepository = await getConnection().getRepository(Contactperson);
        let fetchedProject = await projectRepository.findOne({relations: ["contactperson"], where: { id: updateProject.id }});        

        if(!fetchedProject) {
            return false;
        } else {
            fetchedProject.contactperson.firstname = updateProject.contactperson.firstname;
            fetchedProject.contactperson.lastname = updateProject.contactperson.lastname;
            fetchedProject.contactperson.email = updateProject.contactperson.email;
            fetchedProject.contactperson.date = updateProject.contactperson.date;
            fetchedProject.contactperson.comment = updateProject.contactperson.comment ? updateProject.contactperson.comment : null;
        
            fetchedProject.title = updateProject.title;
            fetchedProject.lecturer = updateProject.lecturer;
            fetchedProject.professor = updateProject.professor;
            fetchedProject.status = updateProject.status;
            fetchedProject.description = updateProject.description;

            await contactpersonRepository.save(fetchedProject.contactperson);
            await projectRepository.save(fetchedProject);

            return true;
        }
    }

}
