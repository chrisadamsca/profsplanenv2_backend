import * as bcrypt from 'bcryptjs';
import { getConnection } from "typeorm";
import { User } from '../entity/User';
import * as jwt from 'jsonwebtoken';
import * as fs from "fs";
import * as path from "path";
import { UserBody } from './models';
import * as expressJwt from "express-jwt";

export default class Auth {

    public static async hashPassword(password: string, rounds: number): Promise<string> {
        const hash = bcrypt.hash(password, rounds);
        return hash;
    }

    public static async compare(password: string, dbHash: string): Promise<boolean> {
        const match = bcrypt.compare(password, dbHash);
        return match;
    }

    public static async login(user: UserBody): Promise<string | null> {
        let userRepository = await getConnection().getRepository(User);
        const fetchedUser = await userRepository.findOne({username: user.username});

        if(!fetchedUser) {
            return null;
        } else {
            const accept = await this.compare(user.password, fetchedUser.password);

            if(accept) {

                const RSA_PRIVATE_KEY = fs.readFileSync(path.resolve(__dirname, "keys/private.key"));

                const jwtBearerToken = jwt.sign({
                    user: fetchedUser.displayName
                }, RSA_PRIVATE_KEY, {
                    algorithm: "RS256",
                    expiresIn: "2h",
                    subject: fetchedUser.username
                });
                
                return jwtBearerToken;

            } else {
                return null;
            }
        }
    }

    public static async logout(): Promise<string> {
        const RSA_PRIVATE_KEY = fs.readFileSync(path.resolve(__dirname, "keys/private.key"));

        const jwtBearerToken = jwt.sign({}, RSA_PRIVATE_KEY, {
            algorithm: "RS256",
            expiresIn: 0,
            subject: "logout"
        });

        return jwtBearerToken;
    }

    public static authenticate(): Function {

        const RSA_PUBLIC_KEY = fs.readFileSync(path.resolve(__dirname, "keys/public.key"));
        const checkIfAuthenticated = expressJwt({
            secret: RSA_PUBLIC_KEY,
            getToken: function fromCookie (req) {                
                if (req.cookies.SESSIONID) {
                    return req.cookies.SESSIONID;
                }
                return null;
              }
        });
        
        return checkIfAuthenticated;
    }
                
        
}