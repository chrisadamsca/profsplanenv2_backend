export interface UserBody {
    id?: number;
    username: string;
    password: string;
    displayName: string;
};

export interface ProjectBody {
    id?: number;
    title: string;
    lecturer: string;
    professor: string;
    description: string;
    status: boolean;
    contactperson: ContactPersonBody;

};

export interface ContactPersonBody {
    id?: number;
    firstname: string;
    lastname: string;
    email: string;
    date: Date;
    comment: string;
};