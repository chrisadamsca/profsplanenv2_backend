import { getConnection } from "typeorm";
import { User } from "../entity/User";
import Auth from "./auth";
import { UserBody } from "./models";

export class ApiUsers{

    private constructor() {};

    // GET user by id
    public static async getOne(id : number): Promise<User> {
        const user = await getConnection().getRepository(User).findOne({id : id});
        return user;
    }

    
    // INSERT given user
    public static async add(newUser: UserBody): Promise<boolean> {
        let userRepository = await getConnection().getRepository(User);
        const fetchedUser = await userRepository.findOne({username: newUser.username});
        
        if(fetchedUser){
            return false;

        } else {
            const user = new User();
            user.username = newUser.username;
            user.password = await Auth.hashPassword(newUser.password, 12);
            user.displayName = newUser.displayName;
            
            await userRepository.save(user);
            return true;
        }

    }
}

