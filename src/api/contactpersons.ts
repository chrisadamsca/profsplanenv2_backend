import { getConnection } from "typeorm";
import { Contactperson } from "../entity/Contactperson";

export class ApiContactpersons {

    private constructor() {};

    // GET all contactpersons
    public static async getAll(): Promise<Contactperson[]> {
        const contactpersons = await getConnection().getRepository(Contactperson).find();
        return contactpersons;
    }

    // GET contactperson by id
    public static async getOne(id: number): Promise<Contactperson> {
        const contactperson = await getConnection().getRepository(Contactperson).findOne({id : id});
        return contactperson; 
    }

}
