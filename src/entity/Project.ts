import {Entity, PrimaryGeneratedColumn, Column, Unique, OneToOne, JoinColumn} from "typeorm";
import { Contactperson } from "./Contactperson";

@Entity()
@Unique(["title"])
export class Project {

    constructor (title: string, lecturer: string, professor: string, description: string, status: boolean, contactperson?: Contactperson) {
        this.title = title;
        this.lecturer = lecturer;
        this.professor = professor;
        this.description = description;
        this.status = status;
        this.contactperson = contactperson || null;
    }

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: false
    })
    title: string;

    @Column({
        nullable: false
    })
    lecturer: string;

    @Column({
        nullable: false
    })
    professor: string;

    @Column({
        nullable: false,
        type: "varchar",
        length: 2048
    })
    description: string;

    @Column({
        nullable: false
    })
    status: boolean;

    @OneToOne(type => Contactperson, { nullable: false })
    @JoinColumn()
    contactperson: Contactperson;

}
