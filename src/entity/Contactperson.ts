import {Entity, PrimaryGeneratedColumn, Column, IsNull, Unique} from "typeorm";
import {Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

@Entity()
@Unique(["firstname", "lastname"])
export class Contactperson {

    constructor (firstname: string, lastname: string, email: string, date: Date, comment?: string) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.date = date;
        this.comment = comment || null;
    }

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: false
    })
    firstname: string;

    @Column({
        nullable: false
    })
    lastname: string;

    @Column({
        nullable: false
    })
    @IsEmail()
    email: string;

    @Column({
        nullable: false
    })
    @IsDate()
    date: Date;

    @Column({
        nullable: true,
        type: "varchar",
        length: 2048
    })
    comment: string;

}
