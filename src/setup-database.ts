import {createConnection} from "typeorm";
import {User} from "./entity/User";
import { Contactperson } from './entity/Contactperson';
import { Project } from './entity/Project';
import Auth from "./api/auth";


createConnection().then(async connection => {

  console.log("Clearing old data.");
  await connection.createQueryBuilder().delete().from(User).execute();
  await connection.createQueryBuilder().delete().from(Project).execute();
  await connection.createQueryBuilder().delete().from(Contactperson).execute();

  const user1 = new User();
  user1.username = "admin";
  user1.password = await Auth.hashPassword('admin', 12);
  user1.displayName = "Prof. Dr. Kleinen";

  const user2 = new User();
  user2.username = "user";
  user2.password = await Auth.hashPassword('admin', 12);
  user2.displayName = "Prof. Dr. Strippgen";

  console.log("Inserting new users into database.");
  let userRepository = connection.getRepository(User);
  await userRepository.save([user1, user2]);

  // adding testdata
  const contactPerson1 = new Contactperson("John", "Doe", "johndoe@htw-berlin.de", new Date("2000-04-03"), "John Doe doesn't exists.");
  const contactPerson2 = new Contactperson("Peter", "Maffay", "maffay@htw-berlin.de", new Date("2018-06-05"));
  const contactPerson3 = new Contactperson("Thorsten", "Thorstenson", "thorstenson@htw-berlin.de", new Date("1990-01-01"));
  const contactPerson4 = new Contactperson("Alan", "Turin", "turing@htw-berlin.de", new Date("2017-09-09"));

  const project1 = new Project("100 Dollar Game", "John Doe", "Prof. Dr. Strippgen", "Use Unity as Engine", false, contactPerson1);
  const project2 = new Project("Web Application with Rails", "Prof. Dr. Kleinen", "Prof. Dr. Kleinen", "Implement a Pizza Delivery Service", true, contactPerson2);
  const project3 = new Project("Image Vectorizer", "Herr Hezel", "Prof. Dr. Jung", "Use JavaFX as Frontend-Tool", true, contactPerson3);
  const project4 = new Project("Do your own thing", "You", "Prof. Dr. Jung", "Research a topic you like", false, contactPerson4);

  let contactpersonRepository = connection.getRepository(Contactperson);
  let projectRepository = connection.getRepository(Project);
 
  console.log("Inserting Testdata into database.");
  await contactpersonRepository.save([contactPerson1, contactPerson2, contactPerson3, contactPerson4]);
  await projectRepository.save([project1, project2, project3, project4]);

  await connection.close();
  return;
  
}).catch(error => console.log(error));
